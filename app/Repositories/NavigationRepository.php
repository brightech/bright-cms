<?php

namespace App\Repositories;

use App\Models\Navigation;

/**
 * Created by PhpStorm.
 * User: samxiao
 * Date: 2017/4/27
 * Time: 下午4:04
 */
class NavigationRepository
{
    public function getSerializedNavigation()
    {
        $navigationList = Navigation::orderBy('parent_id')->orderBy('sorting')->get();
        $result = array();
        foreach ($navigationList as $navigation) {
            if ($navigation->parent_id != 0) {
                $parent = &$result[$navigation->parent_id];
                $parent['items'][$navigation->id] = [
                    'id' => $navigation->id,
                    'label' => $navigation->name,
                    'url' => $navigation->point_url,
                    'items' => []
                ];
            } else {
                $result[$navigation->id] = [
                    'id' => $navigation->id,
                    'label' => $navigation->name,
                    'url' => $navigation->point_url,
                    'items' => []
                ];
            }
        }
        return $result;
    }

    private function serializeNavigation()
    {
        $arr = array();
        foreach ($array as $navigation) {
            if ($navigation['parent_id'] == $pid) {
                $item = $this->formatTree($array, $navigation['id']);
                //判断是否存在子数组
                $item && $navigation['son'] = $item;
                $arr[] = $navigation;
            }
        }
        return $arr;
    }

}