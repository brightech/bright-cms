@php
    $isHome = true;
@endphp
@extends('_site.layouts.app')
@section('title','首页')
@section('content')
    <div class="top-img">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators hidden-xs">
                @foreach($upperAds as $key=>$value)
                    <li class="{{$key == 0 ? 'active' : ''}}" data-target="#carousel-example-generic"
                        data-slide-to="{{$key}}"></li>
                @endforeach
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                @foreach($upperAds as $index => $upperAd)
                    <div class="{{$index == 0 ? 'item active' : 'item'}}">
                        <a href="{{url($upperAd->contenttable->point_url)}}">
                            <img src="{{Storage::url($upperAd->contenttable->assets_url)}}"
                                 alt="...">
                        </a>
                    </div>
                @endforeach

            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <img src="{{asset('/images/left-arrow.png')}}" alt="">
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <img src="{{asset('/images/right-arrow.png')}}" alt="">
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <div class="container-fluid container-middle">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 middle-text"></div>
            </div>

            <div id="owl-demo" class="owl-carousel">
                @foreach($successCase as $key=>$content)
                    <a href="{{url($content->contenttable->point_url)}}">
                        <img src="{{ Storage::url($content->contenttable->assets_url)}}" class="img-responsive"/>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="container-fluid container-news">
        <div class="container">
            <div class="row row-index-news">
                <div class="col-sm-7 col-xs-12 news-text">
                    <div class="news-top">
                        <span>新闻资讯</span>
                    </div>
                    <ul class="list-group news-list-group">
                        @foreach($middleAdBlog as $content)
                            <li class="list-group-item">
                                <a href="{{url('content',['contentId'=>$content->id])}}">{{$content->contenttable->title}}</a>
                                {{date('m-d',strtotime($content->created_at))}}
                            </li>
                        @endforeach
                    </ul>
                    <div class="news-more">
                        <a href="{{url('/list')}}">更多资讯...</a>
                    </div>
                </div>
                <!--  col-sm-offset-1 在当前位置向右移一格   -->
                <div class="col-sm-4 col-sm-offset-1  col-xs-12 news-img">
                    <img alt="" src="{{ Storage::url($middleAdPicture[0]->contenttable->assets_url) }}"
                         class="img-responsive">
                </div>
            </div>
        </div>
    </div>

@stop