@inject('appRepository', 'App\Repositories\AppRepository')
        <!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8" name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no "/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=9"/>

    <meta name="keywords" content="{{$appRepository->getProperty()['keywords']}}">
    <meta name="description" content="{{$appRepository->getProperty()['description']}}">
    <title>{{$appRepository->getProperty()['title']}}-@yield('title')</title>

    <link rel="stylesheet" href="{{asset('/home/css/bootstrap.css')}}"/>
    <link rel="stylesheet" href="{{asset('/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('/home/css/owl.carousel.min.css')}}">
    <style>
        .owl-nav{
            display: block !important;
        }
    </style>
    <script src="{{asset('/home/js/jquery.min.js')}}"></script>
    <script src="{{asset('/home/js/bootstrap.js')}}"></script>
    <script src="{{asset('/home/js/owl.carousel.min.js')}}"></script>

</head>
<body>
<!--nav Start-->

<div class="navbar navbar-default {{ $isHome?'navbar-ths-dark':'navbar-ths-light' }}" role="navigation" id="top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1"
                    aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url('/')}}">
                @if($isHome)
                    <img alt="" src="{{asset('/images/home-logo.png')}}" class="img-responsive hidden-xs">
                @else
                    <img alt="" src="{{asset('/images/logo-dark.png')}}" class="img-responsive hidden-xs">
                @endif
            </a>

        </div>


        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            {{--<ul class="nav nav-pills pull-right">--}}
            <ul class="nav navbar-nav navbar-right">
                <li><a href="//en.thsmedia.com">English</a></li>
            </ul>
            <form class="navbar-form navbar-right hidden-xs " role="search">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="">
                    <button type="submit" class="button-feedback" style="top: 0;">
                        <img alt="" src="{{asset('/images/search2.png')}}" height="16px">
                    </button>
                </div>
            </form>
            <ul class="nav navbar-nav navbar-right" id="navl">
                @foreach($appRepository->getNavigation() as $navigation)
                    <li class="dropdown">
                        @if( empty($navigation['items']))
                            <a href="{{isset($navigation['url']) ? url($navigation['url']):'#'}}">
                                {{$navigation['title']}}
                            </a>
                        @else
                            <a href="{{$navigation['url']}}" class="dropdown-toggle" data-toggle="dropdown">
                                {{$navigation['title']}}
                                <ul class="dropdown-menu">
                                    @foreach($navigation['items'] as $item)
                                        <li>
                                            <a href="{{ url($item['url'] )}}"
                                               tabindex="-1">{{$item['title']}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </a>
                        @endif


                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<!--nav End-->
@yield('content')
<!-- footers-->
@if( !$isHome )
    <div class="container-fluid container-footer-messages">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6  col-md-offset-3">
                    <div class="row row-logo">
                        <div class="col-sm-4 col-xs-12 ">
                            <ul class="list-group">
                                <li class="list-group-item list-title"><img alt="" src="{{asset('/images/03.png')}}">
                                </li>

                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-6">
                            <ul class="list-group">
                                <li class="list-group-item list-title"><img alt="" src="{{asset('/images/02.jpg')}}">
                                </li>
                                <li class="list-group-item">关注我们的微信</li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-6">
                            <ul class="list-group">
                                <li class="list-group-item list-title"><img alt="" src="{{asset('/images/01.png')}}">
                                </li>
                                <li class="list-group-item">关注我们的微博</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row row-link">
                        <div class="col-sm-4 col-xs-4 col-join"><a href="{{url('content/11')}}">加入我们</a></div>
                        <div class="col-sm-4 col-xs-4"><a href="{{url('content/10')}}">法律声明</a></div>
                        <div class="col-sm-4 col-xs-4"><a href="{{url('content/19')}}">商务合作</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="container-fluid container-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-xs-12 footer-logo">
                <img alt="" src="{{asset('/images/logo3.png')}}"
                     class="img-responsive center-block">
            </div>
            <div class="col-sm-5 col-sm-offset-4 col-xs-12 footer-text">
                <span>© 2016太行盛国际文化传媒 版权所有</span><br>
                <span class="bottom-img-right"><a
                            href="http://www.miibeian.gov.cn/">{{$appRepository->getProperty()['icp']}}</a></span>
            </div>
            <div class=" col-sm-1 col-xs-12 back-to-top hidden-xs">
                <a href="javascript:window.scroll(0,0)"><img alt="" src="{{asset('/images/comeback.png')}}"></a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var leftArrowOwl = '<img src="{{asset('/images/left.png')}}" width="40" height="40" alt="">';
    var rightArrowOwl = '<img src="{{asset('/images/right.png')}}" width="40" height="40" alt="">';

    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            margin: 10,
            loop: true,
            dots: false,
            nav: true,
            navText: [leftArrowOwl, rightArrowOwl],
            responsive: {
                '0': {
                    items: 2,
                    nav: false,
                    loop: false
                },
                '992': {
                    items: 4,
                }
            }
        });
    });

    $(function () {
        $("[data-toggle='popover']").popover({
            content: function () {
                return '<img alt="" src="//ths.cdn.daxuefun.cn/images/wechat.jpg" class="img-thumbnail" width="90px" height="90px" >';
            }
        });
    });

</script>
@stack('scripts')
</body>