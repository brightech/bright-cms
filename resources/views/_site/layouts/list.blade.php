@extends('site.layouts.app')
@section('title','列表')
@section('content')
    @include('site.layouts.breadcrumb')
   {{-- <div class="navbar navbar-default navbar-box">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    @foreach($content as $value)
                        <a href="{{url('content/'.$value->id)}}">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    {{$value->contentBlog->title}}
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>--}}
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#One" aria-expanded="true" role="button">
                        <div class="row">
                            <div class="col-xs-11">
                                《怪兽厨房》亮相北京 虚拟+现实引爆产业
                            </div>
                            <div class="col-xs-1"><i class="glyphicon glyphicon-chevron-down"></i>
                            </div>
                        </div>

                    </a>
                </h4>
            </div>
            <div id="One" class="panel-collapse collapse " role="tabpanel" aria-expanded="false">
                <div class="panel-body">
                    11月11日，太行盛国际文化传媒（北京）有限公司（以下简称“太行盛”）全新动漫IP《怪兽厨房》发布会在北京海淀区中关村创业大街洋葱投举行。
                    在发布会上，中国动漫学会副会长兼中国产业联盟副主席王英、北京海置科创服务有限公司副总经理李亚坤、
                    太行盛CEO张亮、COO杜伟时及《怪兽厨房》制作团队代表等均发表了致辞，而备受期待的《怪兽厨房》宣传片也首度亮相。
                    作为国内首部将智能厨房及优质食材合为一体的颠覆性跨界动漫IP，《怪兽厨房》吸引了诸多业界的目光，
                    IP授权和动漫开发者积极跟进，资本和媒体推波助澜，开启了一场虚拟+现实的互动盛宴。
                </div>
            </div>
        </div>

    </div>
@endsection