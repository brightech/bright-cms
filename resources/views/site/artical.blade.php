@extends('site.layouts.app')
@section('title','文章')
@section('content')
    @include('site.layouts.breadcrumb')
    <div class="container project-body">
        <div class="row">
            <div class="project-title">
                <p> {{$content->contentBlog->title}}</p>
                <div class="row">
                    <div class="col-xs-12 container-created">
                       {{date('y-m-d',strtotime($content->created_at))}}		</div>

                </div>
            </div>
            <div class="col-xs-12">
                <div class="project-text">
                    <img src="../storage/{{$content->contentBlog->intro_image}}" alt="">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="project-text">
                    <?=$content->contentBlog->content?>
                </div>
            </div>


        </div>
    </div>
@stop