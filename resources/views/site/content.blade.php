@php
    $isHome = false;
@endphp
@extends('site.layouts.app')
@section('title',$content->categories[0]->name)
@section('content')
    @include('site.layouts.breadcrumb')
    <div class="container">
        <div class="row container-content">
            <div class="col-xs-12">
                <div class="content-title">
                    <p> {{$content->contentBlog->title}}</p>
                    <div class="container-created">
                        {{date('y-m-d',strtotime($content->created_at))}}
                    </div>
                </div>
                <div class="content-text">
                    {!! $content->contentBlog->content !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
  window._bd_share_config = {
    'common': {
      'bdSnsKey': {},
      'bdText': '',
      'bdMini': '2',
      'bdMiniList': false,
      'bdPic': '',
      'bdStyle': '1',
      'bdSize': '16'
    }, 'slide': {'type': 'slide', 'bdImg': '3', 'bdPos': 'right', 'bdTop': '100'}
  }
  with (document)0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion=' + ~(-new Date() / 36e5)]
</script>
@endpush