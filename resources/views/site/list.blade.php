@php
    $isHome = false;
@endphp
@extends('site.layouts.app')
@section('title','列表')
@section('content')
    @include('site.layouts.breadcrumb')
    <div class="container">
        <div class="container-content">
            <div class="panel-group" id="accordion">
                @foreach($content as $value)
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#content{{$value->id}}" aria-expanded="true"
                                   role="button">
                                    <div class="row">
                                        <div class="col-xs-11">
                                            {{$value->contentBlog->title}}
                                        </div>
                                        <div class="col-xs-1"><i class="glyphicon glyphicon-chevron-down"></i>
                                        </div>
                                    </div>
                                </a>
                            </h4>
                        </div>
                        <div id="content{{$value->id}}" class="panel-collapse collapse " role="tabpanel" aria-expanded="false">
                            <div class="panel-body">
                                {!! $value->contentBlog->content !!}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
